package com.example.inan.mymovies;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.inan.mymovies.POJOS.MovieReviewResponse;
import com.example.inan.mymovies.POJOS.Review;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {

    private static final String POSTER_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";
    private static final String POSTER_IMAGE_SIZE = "w500";
    FloatingActionButton favoriteBtn;
    RecyclerView reviewLv;
    RecyclerView.LayoutManager layoutManager;
    public static final String API_KEY = "b33a08ac36419b2ff4427b47276ce5d5";
    ImageView poster, backdrop;
    TextView title,overview,rating,releasedate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        favoriteBtn = (FloatingActionButton) findViewById(R.id.favorite_btn);
        reviewLv = (RecyclerView) findViewById(R.id.user_reviews);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        reviewLv.setLayoutManager(layoutManager);
        poster = (ImageView) findViewById(R.id.movie_detail_poster);
        backdrop = (ImageView) findViewById(R.id.backdrop_image);

        title = (TextView) findViewById(R.id.title_detail_tv);
        overview = (TextView) findViewById(R.id.overview_text_view);
        rating = (TextView) findViewById(R.id.rating);
        releasedate = (TextView) findViewById(R.id.release_date);

        Picasso.with(DetailsActivity.this).load(POSTER_IMAGE_BASE_URL+POSTER_IMAGE_SIZE.concat(intent.getStringExtra("Backdrop")))
                .fit().centerCrop().into(backdrop);

        Picasso.with(DetailsActivity.this).load(POSTER_IMAGE_BASE_URL+POSTER_IMAGE_SIZE.concat(intent.getStringExtra("Poster")))
                .into(poster);

        title.setText(intent.getStringExtra("Title").toString());
        overview.setText(intent.getStringExtra("Overview").toString());
        rating.setText(String.valueOf(intent.getDoubleExtra("Rating",10.0)));
        releasedate.setText(intent.getStringExtra("Release Date").toString());

        ApiInterface apiService =
                APIClient.getApiClient().create(ApiInterface.class);

        Call<MovieReviewResponse> call = apiService.getMovieReviews(intent.getIntExtra("movie_id",1),API_KEY);

        call.enqueue(new Callback<MovieReviewResponse>() {
            @Override
            public void onResponse(Call<MovieReviewResponse> call, Response<MovieReviewResponse> response) {

                List<Review> result = response.body().getResults();
                Log.i("review",result.toString());
                ReviewAdapter adapter = new ReviewAdapter(DetailsActivity.this,result);

                reviewLv.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<MovieReviewResponse> call, Throwable t) {

            }
        });

    }
}
