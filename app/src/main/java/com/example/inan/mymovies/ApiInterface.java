package com.example.inan.mymovies;


import com.example.inan.mymovies.POJOS.MovieResponse;
import com.example.inan.mymovies.POJOS.MovieReviewResponse;
import com.example.inan.mymovies.POJOS.Review;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Inan on 10/14/2017.
 */

public interface ApiInterface {

    @GET("movie/now_playing")
    Call<MovieResponse> getNowPlayingMovies(@Query("api_key") String apiKey);
    @GET("movie/top_rated")
    Call<MovieResponse> getTopRated1Movies(@Query("api_key") String apiKey);
    @GET("movie/popular")
    Call<MovieResponse> getPopularMovies(@Query("api_key") String apiKey);
    @GET("search/movie?")
    Call<MovieResponse> getSearchedMovie(@Query("api_key") String apiKey, @Query("query") String query);
    @GET("movie/{movie_id}/reviews")
    Call<MovieReviewResponse> getMovieReviews(@Path("movie_id") int id, @Query("api_key") String api_key);
}
