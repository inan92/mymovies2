package com.example.inan.mymovies;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.inan.mymovies.POJOS.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Inan on 10/19/2017.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
    private static final String POSTER_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";
    private static final String POSTER_IMAGE_SIZE = "w500";

    private List<Result> result;
    private Context context;
    public Adapter(Context context,List<Result> results){

        this.context = context;
        this.result = results;
    }
    public class Holder extends RecyclerView.ViewHolder{

         ImageView image;
        TextView titleTv;

        public Holder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.movies_item);
            titleTv = itemView.findViewById(R.id.title_tv);
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.card_list_item,parent,false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {


        Picasso.with(context).load(POSTER_IMAGE_BASE_URL+POSTER_IMAGE_SIZE.concat(result.get(position).getPosterPath())).into(holder.image);
        holder.titleTv.setText(result.get(position).getTitle().toString());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,DetailsActivity.class);

                intent.putExtra("movie_id",result.get(position).getId());
                intent.putExtra("Backdrop",result.get(position).getBackdropPath());
                intent.putExtra("Poster",result.get(position).getPosterPath());
                intent.putExtra("Title",result.get(position).getTitle());
                intent.putExtra("Overview",result.get(position).getOverview());
                intent.putExtra("Rating",result.get(position).getVoteAverage());
                intent.putExtra("Release Date",result.get(position).getReleaseDate());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return result.size();
    }










}


