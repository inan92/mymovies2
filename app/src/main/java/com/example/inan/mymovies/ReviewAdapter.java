package com.example.inan.mymovies;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.inan.mymovies.POJOS.Review;

import java.util.List;

/**
 * Created by Inan on 10/21/2017.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.Holder> {

    List<Review> results;
    Context context;

    public ReviewAdapter(Context context,List<Review> results){

        this.context = context;
        this.results = results;
    }
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_review_layout,parent,false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.content.setText(results.get(position).getContent().toString());
        holder.author.setText(results.get(position).getAuthor().toString());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView content;
        TextView author;

        public Holder(View itemView){
            super(itemView);
            content = itemView.findViewById(R.id.review_content);
            author = itemView.findViewById(R.id.review_writer);
        }
    }
}
