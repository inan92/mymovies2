package com.example.inan.mymovies;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.inan.mymovies.POJOS.MovieResponse;
import com.example.inan.mymovies.POJOS.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    String title="My Movies";
    public static final String API_KEY = "b33a08ac36419b2ff4427b47276ce5d5";
    RecyclerView recyclerView;

    Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle(title);
        recyclerView = (RecyclerView) findViewById(R.id.movies_list);

        layoutManager = new GridLayoutManager(MainActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);


        getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.top_rated:
                getTopRatedData();
                ActionBar bar = getSupportActionBar();
                bar.setTitle("Top Rated");
                break;
            case R.id.now_playing:
                getData();
                ActionBar bar2 = getSupportActionBar();
                bar2.setTitle("Now Playing");
                break;

            case R.id.popular:
                getPopularData();
                ActionBar bar3 = getSupportActionBar();
                bar3.setTitle("Popular");
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getData(){
        ApiInterface apiService =
                APIClient.getApiClient().create(ApiInterface.class);

        Call<MovieResponse> call = apiService.getNowPlayingMovies(API_KEY);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

                if (response.code() == 200) {
                    final List<Result> results = response.body().getResults();


                    adapter = new Adapter(MainActivity.this, results);

                    recyclerView.setAdapter(adapter);


                    Log.i("Movies", results.toString());


                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }

        });
    }
    public void getTopRatedData() {
        ApiInterface apiService =
                APIClient.getApiClient().create(ApiInterface.class);

        Call<MovieResponse> call = apiService.getTopRated1Movies(API_KEY);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

                if (response.code() == 200) {
                    final List<Result> results = response.body().getResults();


                    adapter = new Adapter(MainActivity.this, results);

                    recyclerView.setAdapter(adapter);


                    Log.i("Movies", results.toString());


                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }

        });
    }
    public void getPopularData(){
        ApiInterface apiService =
                APIClient.getApiClient().create(ApiInterface.class);

        Call<MovieResponse> call = apiService.getPopularMovies(API_KEY);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

                if (response.code() == 200) {
                    final List<Result> results = response.body().getResults();


                    adapter = new Adapter(MainActivity.this, results);

                    recyclerView.setAdapter(adapter);


                    Log.i("Movies", results.toString());

                 /*   gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            Intent intent = new Intent(MainActivity.this,DetailsActivity.class);

                            intent.putExtra("Poster",results.get(i).getBackdropPath());
                            intent.putExtra("Title",results.get(i).getTitle());
                            intent.putExtra("Overview",results.get(i).getOverview());
                            intent.putExtra("Release Date",results.get(i).getReleaseDate());
                            startActivity(intent);
                        }
                    }); */
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }

        });
    }
}